(function($){

    function init(){
        $("#show-content").on("click", showContent);
        $(".show-more").on("click", showText);
        $("#social").on("click", showSocials);
        $("#facebook, #twitter").on("click", socialDialog);
        if (!("ontouchstart" in document.documentElement)) {
            $("#sound").hover(function() {
                soundFunction();
            });
        }
        else {
            $("#sound").on("click", soundFunction);
        }
        var teamSlider;
        slider();
        
    }
    function showContent() {
        $("#content").css("visibility", "hidden");
        $("#content").show(0, function() {
            teamSlider.reloadSlider();
            $(this).hide();
            $(this).css("visibility", "visible");
        });
        $("#landing").fadeOut(300, function() {
            $("#content").fadeIn(300);
        });
        countNumbers();
    }
    function slider() {
        teamSlider = $(".bxslider").bxSlider({
            nextSelector: "#slider-next",
            prevSelector: "#slider-prev",
            nextText: '<?xml version="1.0" encoding="utf-8"?>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 57.1 44.5" style="enable-background:new 0 0 57.1 44.5;" xml:space="preserve">
                            <g>
                                <g>
                                    <polygon class="arrow" points="34.8,44.5 33.4,43.1 54.2,22.3 33.4,1.5 34.8,0 57.1,22.3        "/>
                                </g>
                                <g>
                                    <rect x="0" y="21.2" class="arrow" width="54.5" height="2.1"/>
                                </g>
                            </g>
                        </svg>',
            prevText: '<?xml version="1.0" encoding="utf-8"?>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 57.1 44.5" style="enable-background:new 0 0 57.1 44.5;" xml:space="preserve">
                            <g>
                                <g>
                                    <polygon class="arrow" points="22.3,0 23.8,1.5 3,22.3 23.8,43.1 22.3,44.5 0,22.3      "/>
                                </g>
                                <g>
                                    <rect x="2.6" y="21.2" class="arrow" width="54.5" height="2.1"/>
                                </g>
                            </g>
                        </svg>',
            pagerCustom: '#pager',
            oneToOneTouch: false,
            onSlideBefore: function() {
                $("#sound").css("pointer-events", "none");
                setTimeout(function() {
                    $(".count").each(function() {
                        $(this).text(0);
                    });
                }, 100);
            },
            onSlideAfter: function() {
                var container = $("body").find("[aria-hidden='false']");
                var team = container.attr("id"); 
                var audio = $("#team-sound");
                audio.find("#ogg").attr("src","audio/"+team+".ogg");
                audio.find("#mp3").attr("src","audio/"+team+".mp3");
                audio[0].pause();
                audio[0].load();
                if($("#sound").hasClass("active")) {
                    $("#sound").css("opacity", ".5");
                    $("#sound").find(".wave").css("fill", "transparent");  
                    $("#sound").removeClass("active");
                }
                $("#sound").css("pointer-events", "auto");
                
                countNumbers();
            }
        });
    }
    function showText() {
        var section = $(this).closest("section");
        if(!section.hasClass("active")) { 
            $(this).addClass("close");
            if($(window).width()>768) {
                section.find("article").css("opacity", "0");
            }
            else {
                section.find("article").fadeOut(300);
            }
            section.find(".more-text").delay(300).fadeIn(300, function() {
                section.addClass("active");
            });
        }
        else {
            $(".close").removeClass("close");
            section.find(".more-text").fadeOut(300, function() {
                if($(window).width()>768) {
                    section.find("article").css("opacity", "1");
                }
                else {
                    section.find("article").fadeIn(300);
                }
                section.removeClass("active");
            });
        }
    }
    function soundFunction() {
        var container = $("#sound");
        if(!container.hasClass("active")) {
            container.css("opacity", "1");
            container.find(".wave").css("fill", "#fff");
            container.find("#team-sound")[0].play();
            container.addClass("active");
        }
        else {
            container.css("opacity", ".5");
            container.find(".wave").css("fill", "transparent");  
            container.find("#team-sound")[0].pause();
            container.find("#team-sound")[0].currentTime = 0;
            container.removeClass("active");
        }   
    }
    function showSocials() {
        $(this).fadeOut(200, function() {
            $("#social-block").fadeIn(200);
        });
    }
    function socialDialog(e) {
        e.preventDefault();
        window.open($(this).attr('href'), 'shareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
        return false;
    }
    function countNumbers() {
        $("body").find("[aria-hidden='false']").find(".count").each(function() {
            var number = $(this);
            jQuery({ Counter: 0 }).animate({ Counter: number.data("number") }, {
                duration: 1500,
                easing: 'swing',
                step: function () {
                    number.text(Math.ceil(this.Counter));
                },
                complete: function() {
                    number.text(number.data("number"));
                }
            });
        });
        
    }
    init();

})(jQuery);